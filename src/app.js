﻿'use strict';

const express = require('express');
const morgan = require('morgan');
const bodyParser = require('body-parser');

const app = express();
app.use(morgan('dev'));

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }));

// parse application/json
app.use(bodyParser.json());

require('./router/routes.js')(app);

// Handle 404
app.use(function(req, res) {
 	res.send({'error' : 'Page not Found (404)'}, 404);
});

app.listen(3000, function () {
	console.log('Example app listening on port 3000!')
});