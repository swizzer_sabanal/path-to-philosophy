﻿'use strict';

const request = require('request');
const cheerio = require('cheerio');
const fs = require('fs');

module.exports = function(app){

	let results, url;

	app.get('/wiki/crawl/:url', function(req, res){
        results = [];
		url = decodeURIComponent(req.params.url);
        console.log('===Begin===');
		process(res);
	});

    function process(res) {
        // Start the request to access the url page
        request(url, function (error, response, body) {
            console.log('Started to crawl: ' + url);
            // Check if no errors occured
            if (!error && response.statusCode == 200) {

                // Utilize the cheerio library on the returned html which will essentially give us jQuery functionality
                let $ = cheerio.load(body);

                // Finally, we'll define the variables we're going to capture
                let json = { url : "", topic : "", content : "", nextUrl : ""};

                // Get the content url of the page
                json.url = url;

                // Get the topic name/title
                json.topic = $('#firstHeading').text();

                // Get the next url to visit
                let wikiLinks = $('#mw-content-text p a[href*="/wiki/"]').toArray();
                for (let indx = 0; indx < wikiLinks.length; indx++) {
                    if (wikiLinks[indx].children[0].data === wikiLinks[indx].children[0].data.toLowerCase()) {
                        json.nextUrl = 'https://en.wikipedia.org' + wikiLinks[indx].attribs.href;
                        break;
                    }
                }

                // Get the content of each topic
                $('#mw-content-text').filter(function(){
                    let data = $(this),
                        rawData = data.html(),
                        content = '';
                    
                    rawData = rawData.split('<div id="toc" class="toc">')[0];
                    let res = rawData.match(/<p>(?:\s*)(.*)(?:\s*)<\/p>{1,4}/g);
                    for (let x = 0; x < res.length; x++) {
                        content += res[x];
                        if (res[x] === '<p></p>') {
                            break;
                        }
                    }
                
                    json.content = content;
                });

                // Store the data scraped from the page in the results array
                results.push(json);
                console.log('Finished');

                // To determine if we are already in the Philosophy page and stop
                if (json.topic !== 'Philosophy') {

                    // Assign the value of the nextUrl
                    url = json.nextUrl;

                    // Recursive call of the function crawl
                    process(res);

                } else {
                    json.nextUrl = '---'

                    console.log('===End===');

                    //write to file
                    fs.writeFile('output.json', JSON.stringify(results, null, 4), function(err){
                        console.log('JSON result can also be viewed in output.json file inside the exam folder.');
                    })

                    res.send(results);
                }
            }
        });  
    }
}